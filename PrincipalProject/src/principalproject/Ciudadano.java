/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principalproject;

/**
 *
 * @author mnnun
 */
public class Ciudadano extends Agente{
    private float nivelAgravio;
    private float perjuicioRecibido;
    private float aversionRiesgo;
    private float riesgoNeto;
    private String estado;

    public Ciudadano() {
    }

    public float getNivelAgravio() {
        return nivelAgravio;
    }

    public void setNivelAgravio(float nivelAgravio) {
        this.nivelAgravio = nivelAgravio;
    }

    public float getPerjuicioRecibido() {
        return perjuicioRecibido;
    }

    public void setPerjuicioRecibido(float perjuicioRecibido) {
        this.perjuicioRecibido = perjuicioRecibido;
    }

    public float getAversionRiesgo() {
        return aversionRiesgo;
    }

    public void setAversionRiesgo(float aversionRiesgo) {
        this.aversionRiesgo = aversionRiesgo;
    }

    public float getRiesgoNeto() {
        return riesgoNeto;
    }

    public void setRiesgoNeto(float riesgoNeto) {
        this.riesgoNeto = riesgoNeto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
