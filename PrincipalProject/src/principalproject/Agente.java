/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principalproject;

/**
 *
 * @author mnnun
 */
public class Agente {
    private int rangoVision;
    private float legitimidadGobierno;

    public Agente() {
    }

    public int getRangoVision() {
        return rangoVision;
    }

    public void setRangoVision(int rangoVision) {
        this.rangoVision = rangoVision;
    }

    public float getLegitimidadGobierno() {
        return legitimidadGobierno;
    }

    public void setLegitimidadGobierno(float legitimidadGobierno) {
        this.legitimidadGobierno = legitimidadGobierno;
    }
    
}
