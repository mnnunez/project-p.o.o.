/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principalproject;

/**
 *
 * @author mnnun
 */
public class Universo {
    public int k;
    public float limite;
    private int maxTiempoCarcel;
    private int dias;
    private Agente[][] dimension;
    private int densidadPolicias;
    private int densidadCiudadanos;
    private boolean movimiento;

    public Universo() {
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
    }

    public float getLimite() {
        return limite;
    }

    public void setLimite(float limite) {
        this.limite = limite;
    }

    public int getMaxTiempoCarcel() {
        return maxTiempoCarcel;
    }

    public void setMaxTiempoCarcel(int maxTiempoCarcel) {
        this.maxTiempoCarcel = maxTiempoCarcel;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }

    public Agente[][] getDimension() {
        return dimension;
    }

    public void setDimension(Agente[][] dimension) {
        this.dimension = dimension;
    }

    public int getDensidadPolicias() {
        return densidadPolicias;
    }

    public void setDensidadPolicias(int densidadPolicias) {
        this.densidadPolicias = densidadPolicias;
    }

    public int getDensidadCiudadanos() {
        return densidadCiudadanos;
    }

    public void setDensidadCiudadanos(int densidadCiudadanos) {
        this.densidadCiudadanos = densidadCiudadanos;
    }

    public boolean isMovimiento() {
        return movimiento;
    }

    public void setMovimiento(boolean movimiento) {
        this.movimiento = movimiento;
    }
    
}
