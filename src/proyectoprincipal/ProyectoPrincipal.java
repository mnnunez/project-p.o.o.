/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoprincipal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author M.Nuñez, S.Alcivar, L.Chávez
 */
public class ProyectoPrincipal {

    /**
     * @param args the command line arguments
     */
    public static void generarArchivoCSV(){
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy_HHmmss");
        Date date = new Date();
        String nombreArchivo = "simulacion_"+dateFormat.format(date)+".csv";
        try {
            String texto = "'rio', 'ramo', 'raza'";
            String ruta = nombreArchivo;            
            File file = new File(ruta);
            FileWriter fw = new FileWriter(file); 
            BufferedWriter bw = new BufferedWriter(fw);
            
            fw.write(texto);
            fw.close();            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int filas=0;
        int columnas=0;
        int rangoVision=1;
        System.out.println("Ingrese el tamaño de filas para el Universo: ");
        filas= sc.nextInt();
        System.out.println("Ingrese el tamaño de columnas para el Universo: ");
        columnas= sc.nextInt();
        System.out.println("Ingrese el rango de vision de los agentes: ");
        rangoVision = sc.nextInt();
        while (filas>=50 | columnas>=50){
            System.out.println("ALERTA!!!");
            System.out.println("Ha ingresado un valor fuera del permitido");
            System.out.println("El valor máximo a ingresar es de 50, tanto para filas y columnas");
            System.out.println("Ingrese el tamaño de filas para el Universo: ");
            filas= sc.nextInt();
            System.out.println("Ingrese el tamaño de columnas para el Universo: ");
            columnas= sc.nextInt();
        }
        System.out.println("Ingrese la cantidad que días que desee que dure la sublevación: ");
        int dias=sc.nextInt();
        Universo universe= new Universo(dias,filas,columnas, rangoVision);
        universe.cargarPolicia();
        universe.cargarCiudadano();
        
        
        
        //generarArchivoCSV();
        }
        
    }
    
