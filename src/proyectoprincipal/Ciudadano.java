/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoprincipal;

import static java.lang.Math.exp;
import static java.lang.Math.round;
import java.util.Random;

/**
 *
 * @author M.Nuñez, S.Alcivar, L.Chávez
 */
public class Ciudadano extends Agente {
    private float PERJUICIO_PERCIBIDO; // constante al instanciar el objeto
    private float AVERSION_RIESGO;
    private float detencionEstimada;
    
    private float agravio; //     
    private float riesgoNeto;
    
    private String estado;
    private float densidadInicialCiudadanos;
    
    public Ciudadano() {
        this.PERJUICIO_PERCIBIDO = (float) Math.random();
        this.AVERSION_RIESGO = (float) Math.random();
    }
    
    
    public void obtenerNivelAgravio(float LEGITIMIDAD_GOBIERNO){
        this.agravio = PERJUICIO_PERCIBIDO * (1 - LEGITIMIDAD_GOBIERNO);
        
    }
    
    public void obtenerProbabilidadDetencionEstimada( float k, int C, int A) {
        this.detencionEstimada = (float) (1 - exp(-k*round(C/A)));
    }
    
    public void obtenerRiesgoNeto() {
        this.riesgoNeto = this.AVERSION_RIESGO * this.detencionEstimada;
    }
    
    public boolean rebelarse(float LIMITE) {
        boolean valor = this.agravio - this.riesgoNeto > LIMITE;
        return valor;
    }
    
    
    
    
}
