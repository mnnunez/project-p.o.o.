/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoprincipal;

import java.util.Scanner;
import java.util.Random;

/**
 *
 * @author M.Nu�ez, S.Alcivar, L.Ch�vez
 */
public class Universo {
    public int k;
    public float limite;
    private int maxTiempoCarcel;
    private int dias;
    private Agente[][] dimension;
    private int densidadPolicias;
    private int densidadCiudadanos;
    private boolean movimiento;
    private int tamanoFila;
    private int tamanoColumna;
    private int numeroTurno;
    private int rangoVision;
    private Agente[] carcel; 
    
    
    public Universo(int dias,int tamanoFila,int tamanoColumna, int rangoVision) {
        this.dias=dias;
        this.tamanoFila= tamanoFila;
        this.tamanoColumna=tamanoColumna;
        this.dimension=new Agente[tamanoFila][tamanoColumna];
        this.numeroTurno = 1;
        this.rangoVision = rangoVision;
    }

    public void cargarPolicia(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese la densidad de policias a crear: ");
        densidadPolicias= sc.nextInt();
        int cantidadPolicias= (int)(((float)densidadPolicias/100f)*((float)tamanoFila*(float)tamanoColumna));
        System.out.println("La cantidad de policias es: "+cantidadPolicias);
        int contador= 0;
        Random r= new Random();
        while(contador<=cantidadPolicias){
            int posicionAleatoriaFila=r.nextInt(tamanoFila);
            int posicionAleatoriaColumna=r.nextInt(tamanoColumna);
            if (dimension[posicionAleatoriaFila][posicionAleatoriaColumna]== null){
                dimension[posicionAleatoriaFila][posicionAleatoriaColumna]= new Policia();
                contador++;
            }
        }
    }
    
    public void cargarCiudadano(){
        int densidadMaxDisponible= 100-densidadPolicias;
        System.out.println("La m�xima densidad de Ciudadanos que puede crear es: "+densidadMaxDisponible);
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Ingrese la densidad de ciudadanos a crear: ");
        densidadCiudadanos= sc1.nextInt();
        int cantidadCiudadanos= (int)(((float)densidadCiudadanos/100f)*((float)tamanoFila*(float)tamanoColumna));
        System.out.println("La cantidad de ciudadanos es: "+cantidadCiudadanos);
        int contador= 0;
        Random r= new Random();
        while(contador<=cantidadCiudadanos){
            int posicionAleatoriaFila=r.nextInt(tamanoFila);
            int posicionAleatoriaColumna=r.nextInt(tamanoColumna);
            if (dimension[posicionAleatoriaFila][posicionAleatoriaColumna]== null){
                dimension[posicionAleatoriaFila][posicionAleatoriaColumna]= new Ciudadano();
                contador++;
            }
        }
    }
    
    
    public void movimiento(Agente agent) {
        for (int x = 0; x < this.dimension.length;x++) {
            for (int y = 0; y < this.dimension[x].length; y++) {
                
                int aux1 = (x + this.rangoVision <= this.tamanoFila)? x + this.rangoVision : this.tamanoFila;                
                int aux2 = (x - this.rangoVision >= 0)? x - this.rangoVision : 0;
                
                int auy2 = (y + this.rangoVision <= this.tamanoColumna)? x + this.rangoVision: this.tamanoColumna;
                int auy1 = (y - this.rangoVision >= 0)? x - this.rangoVision : 0;
                
                
            }   
        }
    }
    
    public int getRangoVision() {
        return rangoVision;
    }

    public void setRangoVision(int rangoVision) {
        this.rangoVision = rangoVision;
    }
    
    
    
    
}
